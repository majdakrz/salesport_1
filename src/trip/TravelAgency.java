package trip;
import java.util.ArrayList;
import salesport.SalesPort;
import trip.Trip;
import trip.LongDistanceTrip;
import trip.ShortDistanceTrip;

import java.util.Objects;
import java.util.Scanner;

public class TravelAgency {
    Scanner scanner;

    public TravelAgency(Scanner scanner) {
        this.scanner = scanner;
    }

  /*  private Trip[] shortTrips =
            {

            };
*/
    ArrayList<Trip> trips = new ArrayList<Trip>();
        {
            trips.add(new ShortDistanceTrip("Brno"));
            trips.add(new LongDistanceTrip("United States Adventure",50));
            trips.add(new LongDistanceTrip("Warsaw trip",500,3));
            trips.add(new LongDistanceTrip("Wroclaw trip",300,3));
        };


    public void processRequest(String line){

        /* Task: implement option "list trips"
         *   - the first line will contain a text with the number of offered trips.
         *   - each following line will contain the name and price of the trip. As part of this solution
         *       - create a Trip constructor with an argument of the name and adjust the code accordingly
         *       - override the method toString (Object method) in the Trip
         */

      switch(line) {
          case "list trips":
              listTrips();

              break;
          case "list salesport":
              //listSalesPort();
              break;

          case "add trip":
             addTrip();
            break;
        default:
            System.out.println("Input '"+line+"' is invalid argument");
           // throw new IllegalArgumentException("Input '" + line + "' is not a valild entry.");
        }
    }

   /* private void listSalesPort() {
        System.out.println("SE Travel Agency has promotion on: ");
        for (Trip trip : shortTrips) {
            if (trip instanceof SalesPort) {
                SalesPort promoTrip = (SalesPort) trip;
                System.out.println(promoTrip.getPromoName() + " for " + promoTrip.getPromoPrice() + " CZK!");
            }
        }
        for (Trip trip : longTrips) {
            if (trip instanceof SalesPort) {
                SalesPort promoTrip = (SalesPort) trip;
                System.out.println(promoTrip.getPromoName() + " for " + promoTrip.getPromoPrice() + " CZK!");
            }
        }
    }
*/

    private void listTrips() {


        for (Trip i : trips)
        {
            System.out.println(i.getName()+" "+ i.getPrice()+" CZK");
        }


        /*System.out.println("Long trips: ");
        for (Trip i : longTrips) {
            System.out.println(i.getName()+" "+ i.getPrice()+" CZK");
        }*/
    }

    private void addTrip()
    {
        System.out.println("Want you add a LONG or SHORT distance trip?");
        String line = scanner.nextLine();
        if (Objects.equals(line, "long"))
        {
            System.out.println("Insert the name of a trip:");
            line = scanner.nextLine();
            System.out.println("Insert the distance of a trip:");
            int distance = Integer.parseInt(scanner.nextLine());
            trips.add(new LongDistanceTrip(line, distance));
            System.out.println("Trip called: "+line+" has added successfully!");
        }
        else if (Objects.equals(line, "short"))
        {
            System.out.println("Insert the name of a trip:");
            line = scanner.nextLine();
            trips.add(new ShortDistanceTrip(line));
            System.out.println("Trip called: "+line+" has added successfully!");
        }
        else
            System.out.println(line+" DUPAAAAA");


    }
}

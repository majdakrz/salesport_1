package trip;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TravelAgency agency = new TravelAgency(scanner);
        String line;

        System.out.println("Welcome to SE Travel Agency!");
        while(true) {
            line = scanner.nextLine();
            if (line.toLowerCase().equals("quit")) {
                System.out.printf("Bye, bye!");
                break;
            }

            /* Task: the method processRequest can throw an exception. Handle the exception. */
            agency.processRequest(line);
        }
    }
}

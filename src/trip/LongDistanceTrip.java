package trip;

import salesport.SalesPort;

public class LongDistanceTrip extends Trip implements SalesPort {
    private int distance;
    private int durationInDays;
    private String name;

    public LongDistanceTrip(String name, int distance) {
        this.name = name;
        this.distance = distance;
        this.durationInDays = 1;
    }



    public LongDistanceTrip(String name, int distance, int durationInDays) {
        this.durationInDays = durationInDays;
        this.name = name;
    }

    @Override
    public int getCapacity() {
        return 48;
    }

    @Override
    public double getPrice() {
        return 350*durationInDays + 4*distance*2;
    }

    @Override
    public double getPromoPrice() {
        return getPrice()*0.9;
    }

    @Override
    public String getPromoName() {
        return "Awesome " + durationInDays + "-day trip";
    }

    @Override
    public String getName()
    {
        return name;
    }
}

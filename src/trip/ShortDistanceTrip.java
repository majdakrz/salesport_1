package trip;

public class ShortDistanceTrip extends Trip {
    private String name;

    public ShortDistanceTrip(String name)
    {
        this.name = name;
    }

    @Override
    public int getCapacity() {
        return 35;
    }

    @Override
    public double getPrice() {
        double basePrice = 300;
        double guideShare = 0.15;

        return basePrice + basePrice*guideShare;
    }

    @Override
    public String getName()
    {
        return name;
    }
}

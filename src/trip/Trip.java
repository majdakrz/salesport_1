package trip;

public abstract class Trip {
    private String name;
    private String description;

    public abstract int getCapacity();

    public abstract double getPrice();


    // IDEA code generation: Code -> Generate... -> Getter -> highlight all -> OK

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString()
    {
       return name;
    };

}
